package bookstore;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Entity
@Table(name = "authors")
public class Author {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;

    @Column(name = "first_name")
    public String firstName;

    @Column(name = "last_name")
    public String lastName;

    public String getLastName() {
        return lastName;
    }

    @Column(name = "date_of_birth")
    public LocalDate dateOfBirth;

    @Enumerated(EnumType.STRING)
    public Sex sex;

    @ManyToMany(mappedBy = "authors")
//    @JoinTable(name = "books_authors",
//            joinColumns = @JoinColumn(name = "book_id"),
//            inverseJoinColumns = @JoinColumn(name = "author_id")
//    )
    List<Book> books;

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Author{");
        sb.append("id=").append(id);
        sb.append(", firstName='").append(firstName).append('\'');
        sb.append(", lastName='").append(lastName).append('\'');
        sb.append(", dateOfBirth=").append(dateOfBirth);
        sb.append(", sex=").append(sex);
        sb.append('}');
        return sb.toString();
    }
}

