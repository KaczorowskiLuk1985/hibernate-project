package bookstore;

import javax.persistence.*;
import java.util.List;
import java.util.Locale;

@Entity
@Table(name = "books")
public class Book {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Integer id;
    public String title;

    @Column(name = "pages_number")
    public Integer pagesNumber;
    public Long isbn;

    @ManyToOne
    @JoinColumn(name = "category_id")
    Category category;

    //    @ManyToMany(mappedBy = "books")
    @ManyToMany
    @JoinTable(name = "books_authors",
            inverseJoinColumns = @JoinColumn(name = "author_id"),
            joinColumns = @JoinColumn(name = "book_id")
    )
    List<Author> authors;

    @Override
    public String toString() {
        return "Book{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", pagesNumber=" + pagesNumber +
                ", isbn=" + isbn +
//                ", category=" + category +
//                ", authors=" + authors +
                '}' + System.lineSeparator();
    }
}
