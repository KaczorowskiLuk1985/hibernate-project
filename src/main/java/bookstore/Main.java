package bookstore;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Main {
    public static void main(String[] args) {
        Logger.getLogger("org.hibernate").setLevel(Level.SEVERE);
        EntityManagerFactory factory = Persistence.createEntityManagerFactory("bookstore");
        EntityManager entityManager = factory.createEntityManager();

        List<Author> author = entityManager
                .createQuery("from Author where lastName = :name", Author.class)
                .setParameter("name", "Sapkowski")
                .getResultList();
//
        System.out.println(author);
//
//        List<Category> categories = entityManager
//                .createQuery("from Category where name =:name", Category.class)
//                .setParameter("name", "Horror")
//                .getResultList();
//        System.out.println(categories);
//
//        List<Category> categories1 = entityManager
//                .createQuery("from Category", Category.class)
//                .getResultList();
//        System.out.println(categories1);
//
//        List<Format> format = entityManager
//                .createQuery("from Format where name =:name", Format.class)
//                .setParameter("name", "PDF")
//                .getResultList();
//
//        System.out.println(format);
//
//        List<Book> book = entityManager
//                .createQuery("from Book where title =:name ", Book.class)
//                .setParameter("name", "Marsjanin")
//                .getResultList();
//
//        for (Book book1 : book) {
//            System.out.println(book1.authors);
//        }

//        List<Category>categories = entityManager
//                .createQuery("from Category where name =:name",Category.class)
//                .setParameter("name","Horror")
//                .getResultList();
//        for (Category category : categories) {
//            System.out.println(category.books);
//        }

//        List<Book> booksPages = entityManager
//                .createQuery("from Book where pagesNumber > 500",Book.class)
//                .getResultList();
//        for (Book booksPage : booksPages) {
//            System.out.println(booksPage);
//        }


//        List<Author> authors = entityManager
//                .createQuery("from Author where lastName =: name", Author.class)
//                .setParameter("name", "king")
//                .getResultList();
//
//        for (Author author : authors) {
//            System.out.println(author.books);
//        }

    }
}

